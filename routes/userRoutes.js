const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


//USER REGISTRATION
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})




module.exports = router;