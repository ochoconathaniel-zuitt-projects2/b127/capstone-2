const express = require('express');
const mongoose = require('mongoose')
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.mv91k.mongodb.net/Batch127_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open',() => console.log('Now Connected To MongoDb Atlas !'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use('/Users', userRoutes);
app.use('/Products', productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
} )