const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

firstName : {
		type: String,
		required: [true, 'First name is required']
	},


lastName: {

		type:String,
		required: [true, "Please fill up your last name"]

},

email: {

		type:String,
		required: [true, "Please fill up your email"]


},

password: {

		type:String,
		required: [true, "Please fill up your password"]

},


isAdmin: {

		type:Boolean,
		default: false
}



/*city: {

		type:String,
		required:[true, "City is required"]
	},


completeAfdress: {

		type:String,
		required:[true, 'Complete address is required']
	}





*/






})

module.exports = mongoose.model('User', userSchema);
