const User = require('../models/userModel')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Product = require('../models/productModel');


module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName:  reqBody.lastName,
		email: 	   reqBody.email,
		password:  bcrypt.hashSync(reqBody.password, 10)
	
	})

	return newUser.save().then((user, error)=> {
	
		if(error){
			console.log(error);
			return false;
		}else{
			return true;
		}
	})
}




